<?php

use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Customer\ClienteController;
use App\Http\Controllers\Customer\ClienteCuentaController;
use App\Http\Controllers\Customer\ClienteTarjetaController;
use App\Http\Controllers\Tarjeta\TarjetaController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['prefix' => 'auth'], function () {
    Route::post('register/natural', [RegisterController::class, 'registerNatural'])
        ->name('aut.register.natural');
    Route::post('register/juridico', [RegisterController::class, 'registerJuridico'])
        ->name('aut.register.juridico
        ');
});

Route::group(['middleware' => 'auth:api'], function () {
    Route::group(['prefix' => 'customer'], function () {
        Route::get('info', [ClienteController::class, 'resume']);
        Route::get('account/resume', [ClienteCuentaController::class, 'getSaldoCuenta']);
        Route::get('card/resume', [ClienteTarjetaController::class, 'getSaldoTarjerta']);
    });

    Route::group(['prefix' => 'payment'], function (){
        Route::post('/pay',[TarjetaController::class,'payCard'])->name('payment.pay');
    });
});
