<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogTransaccion extends Model
{
    use HasFactory;

    protected $fillable = [
        'fecha_transaccion',
        'fecha',
        'hora',
        'monto_transaccion'
    ];

    protected $guarded = [
        'id',
        'tipo_transaccion_id',
        'estado_transaccion_id',
        'tarjeta_id',
        'cuenta_id_acreedor',
        'cuenta_id_deudor'
    ];
}
