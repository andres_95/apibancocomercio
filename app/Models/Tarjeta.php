<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tarjeta extends Model
{
    use HasFactory;

    protected $fillable = [
        'numero_tarjeta',
        'ccv',
        'fecha_vencimiento',
        'saldo_disponible',
        'limite_saldo',
        'deuda_actual'
    ];

    protected $guarded = [
        'id',
        'cliente_id'
    ];


    public function cliente()
    {
        return $this->belongsTo(Cliente::class,'cliente_id');
    }
}
