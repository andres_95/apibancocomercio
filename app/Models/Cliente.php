<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\True_;

class Cliente extends Model
{
    use HasFactory;

    protected $fillable = [
        'tipo_dni',
        'dni',
        'tipo_cliente_id',
        'api_key'
    ];

    protected $guarded = [
        'user_id',
        'id'
    ];

    public function cuenta()
    {
        return $this->hasOne(Cuenta::class, 'cliente_id');
    }

    public function tarjeta()
    {
        return $this->hasOne(Tarjeta::class, 'cliente_id');
    }

    public function tipo()
    {
        return $this->belongsTo(TipoCliente::class,'tipo_cliente_id');
    }
}
