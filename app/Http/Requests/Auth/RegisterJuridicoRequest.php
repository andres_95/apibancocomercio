<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class RegisterJuridicoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|string|unique:users|max:20',
            'password' => 'required|string|max:20',
            'nombres' => 'required|string|max:80',
            'type_dni' => 'required|string|max:2',
            'dni' => 'required|numeric',
            'email' => 'required|email|unique:users'
        ];
    }
}
