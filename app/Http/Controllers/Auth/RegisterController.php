<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterJuridicoRequest;
use App\Http\Requests\Auth\RegisterNaturalRequest;
use App\Models\Cuenta;
use App\Models\Role;
use App\Models\Tarjeta;
use App\Models\TipoCliente;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    //
    public function registerNatural(RegisterNaturalRequest $request)
    {
        $role = Role::query()->where('name', 'cliente')->first();
        $tipo_cliente = TipoCliente::query()->where('descripcion', 'natural')->first();

        $user = User::query()->create([
            'username' => $request->get('username'),
            'password' => Hash::make($request->get('password')),
            'first_name' => $request->get('nombres'),
            'last_name' => $request->get('apellidos'),
            'email' => $request->get('email'),
            'is_active' => 1,
            'date_joined' => now(),
            'role_id' => $role->id
        ]);

        if ($user) {
            $cliente = $user->cliente()->create([
                'tipo_dni' => $request->get('type_dni'),
                'dni' => $request->get('dni'),
                'tipo_cliente_id' => $tipo_cliente->id,
            ]);

            if ($cliente) {
                $numero_cuenta = '0194' . strval(rand(1000, 9999)) . strval(rand(1000, 9999)) . strval(rand(1000, 9999));
                $check = Cuenta::query()->where('numero_cuenta', $numero_cuenta)->first();
                while ($check) {
                    $numero_cuenta = '0194' . strval(rand(1000, 9999)) . strval(rand(1000, 9999)) . strval(rand(1000, 9999));
                    $check = Cuenta::query()->where('numero_cuenta', $numero_cuenta)->first();
                }

                $cuenta = $cliente->cuenta()->create([
                    'cliente_id' => $cliente->id,
                    'numero_cuenta' => $numero_cuenta,
                    'saldo_actual' => 10000
                ]);

                $numero_tarjeta = strval(rand(1000, 9999)) . strval(rand(1000, 9999)) . strval(rand(1000, 9999));
                $check = Tarjeta::query()->where('numero_tarjeta', $numero_tarjeta)->first();
                while ($check) {
                    $numero_tarjeta = strval(rand(1000, 9999)) . strval(rand(1000, 9999)) . strval(rand(1000, 9999));
                    $check = Tarjeta::query()->where('numero_tarjeta', $numero_tarjeta)->first();
                }

                $tarjeta = $cliente->tarjeta()->create([
                    'numero_tarjeta' => $numero_tarjeta,
                    'ccv' => rand(100, 999),
                    'fecha_vencimiento' => now()->addYears(3),
                    'saldo_disponible' => 1000,
                    'limite_saldo' => 1000,
                    'deuda_actual' => 0
                ]);

                return response()->json(null, 201);
            }
        }
        return response()->json(null, 422);
    }

    public function registerJuridico(RegisterJuridicoRequest $request)
    {
        $role = Role::query()->where('name', 'cliente')->first();
        $tipo_cliente = TipoCliente::query()->where('descripcion', 'juridico')->first();

        $user = User::query()->create([
            'username' => $request->get('username'),
            'password' => Hash::make($request->get('password')),
            'first_name' => $request->get('nombres'),
            'email' => $request->get('email'),
            'is_active' => 1,
            'date_joined' => now(),
            'role_id' => $role->id
        ]);

        if ($user) {
            $cliente = $user->cliente()->create([
                'tipo_dni' => $request->get('type_dni'),
                'dni' => $request->get('dni'),
                'tipo_cliente_id' => $tipo_cliente->id,
            ]);

            if ($cliente) {
                $numero_cuenta = '0194' . strval(rand(1000, 9999)) . strval(rand(1000, 9999)) . strval(rand(1000, 9999));
                $check = Cuenta::query()->where('numero_cuenta', $numero_cuenta)->first();
                while ($check) {
                    $numero_cuenta = '0194' . strval(rand(1000, 9999)) . strval(rand(1000, 9999)) . strval(rand(1000, 9999));
                    $check = Cuenta::query()->where('numero_cuenta', $numero_cuenta)->first();
                }

                $cuenta = $cliente->cuenta()->create([
                    'cliente_id' => $cliente->id,
                    'numero_cuenta' => $numero_cuenta,
                    'saldo_actual' => 10000
                ]);

                $cuenta->api_key = $user->createToken($user->first_name)->accessToken;
                $cuenta->save();

                return response()->json(null, 201);
            }
        }
        return response()->json(null, 422);
    }
}
