<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ClienteController extends Controller
{
    public function resume()
    {
        $user = Auth::user();
        $user->load(['cliente.tipo','role']);

        return response()->json($user);
    }
}
