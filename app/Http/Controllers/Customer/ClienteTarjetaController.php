<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ClienteTarjetaController extends Controller
{
    //
    public function getSaldoTarjerta()
    {
        $user = Auth::user();

        $cuenta = $user->load('cliente.tarjeta');

        return response()->json($cuenta->cliente->tarjeta);
    }
}
