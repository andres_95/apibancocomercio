<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ClienteCuentaController extends Controller
{
    //

    public function getSaldoCuenta()
    {
        $user = Auth::user();

        $cuenta = $user->load('cliente.cuenta');

        return response()->json($cuenta->cliente->cuenta);
    }
}
