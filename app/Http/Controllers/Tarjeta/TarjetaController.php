<?php

namespace App\Http\Controllers\Tarjeta;

use App\Http\Controllers\Controller;
use App\Models\Tarjeta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class TarjetaController extends Controller
{
    //

    public function payCard(Request $request)
    {
        $user = Auth::user();
        $role = $user->role;
        $tipo_cliente = $user->cliente->tipo;

        if ($role->name !== 'cliente' || $tipo_cliente->descripcion !== 'juridico' ){
            Validator::make([], [])
                ->after(function ($validator) {
                    $validator->errors()->add(
                        'blocked', 'La tienda no esta autorizada para procesar pagos'
                    );
                })->validate();
        }

        $tarjeta = Tarjeta::query()
            ->where('numero_tarjeta',$request->get('numero'))
            ->where('ccv',$request->get('ccv'))
            ->first();

        if (!$tarjeta){
            Validator::make([], [])
                ->after(function ($validator) {
                    $validator->errors()->add(
                        'blocked', 'Tarjeta Invalida'
                    );
                })->validate();
        }

        if($tarjeta->saldo_disponible < $request->get('monto')){
            Validator::make([], [])
                ->after(function ($validator) {
                    $validator->errors()->add(
                        'rejected', 'Saldo Insuficiente'
                    );
                })->validate();
        }

        $cuenta = $user->cliente->cuenta;
        $cuenta->saldo_actual += $request->get('monto');
        $tarjeta->saldo_disponible -= $request->get('monto');
        $tarjeta->deuda_actual += $request->get('monto');

        $tarjeta->save();
        $cuenta->save();

        return response()->json();

    }
}
