<?php

use App\Models\Cliente;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTarjetaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tarjetas', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Cliente::class)
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('restrict');
            $table->decimal('numero_tarjeta',16,0,true);
            $table->integer('ccv')->unsigned();
            $table->date('fecha_vencimiento');
            $table->decimal('saldo_disponible',12,2);
            $table->decimal('limite_saldo',12,2,true);
            $table->decimal('deuda_actual',12,2,true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tarjetas');
    }
}
