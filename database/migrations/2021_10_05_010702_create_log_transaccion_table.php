<?php

use App\Models\Cliente;
use App\Models\EstadoTransaccion;
use App\Models\Tarjeta;
use App\Models\TipoTransaccion;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogTransaccionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_transaccions', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(TipoTransaccion::class)
                ->constrained()
                ->onUpdate('restrict')
                ->onDelete('restrict');
            $table->foreignIdFor(EstadoTransaccion::class)
                ->constrained()
                ->onUpdate('restrict')
                ->onDelete('restrict');
            $table->foreignIdFor(Tarjeta::class)
                ->nullable()
                ->constrained()
                ->onUpdate('restrict')
                ->onDelete('restrict');
            $table->foreignIdFor(Cliente::class,'cuenta_id_acreedor')
                ->nullable()
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('restrict');
            $table->foreignIdFor(Cliente::class,'cuenta_id_deudor')
                ->nullable()
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('restrict');
            $table->dateTime('fecha_transaccion');
            $table->date('fecha');
            $table->time('hora');
            $table->decimal('monto_transaccion',12,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_transaccions');
    }
}
