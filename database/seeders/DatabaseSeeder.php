<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\TipoCliente;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        TipoCliente::factory()->create([
            'descripcion' => 'natural'
        ]);
        TipoCliente::factory()->create([
            'descripcion' => 'juridico'
        ]);

        Role::factory()->create([
           'name' => 'master'
        ]);
        Role::factory()->create([
           'name' => 'cliente'
        ]);
    }
}
